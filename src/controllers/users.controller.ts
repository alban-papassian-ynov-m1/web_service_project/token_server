import { Controller, HttpCode, Param, Get, Post, Body, Delete } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { FindOneOptions } from 'typeorm';
import { UsersService } from '../services/users.service';
import { GetUserResponse, GetUsersResponse } from '../models/responses/user.responses';
import { User } from '../entities/user.entity';
import { UserDto } from '../models/dto/user.dto';
import { GenericResponse } from '../models/responses/generic-response.responses';

@Controller('users')
@ApiUseTags('users')
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
    ) { }

    @Get(':id')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get category', operationId: 'getCategory' })
    @ApiResponse({ status: 200, description: 'Get category', type: GetUserResponse })
    @HttpCode(200)
    async get(@Param('id') userId: string): Promise<GetUserResponse> {
        const findOneOptions: FindOneOptions<User> = { where: { id: userId } };
        return await this.usersService.findOne(findOneOptions);
    }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all users', operationId: 'getAllUsers' })
    @ApiResponse({ status: 200, description: 'Get all users', type: GetUsersResponse })
    @HttpCode(200)
    async getAll(): Promise<GetUsersResponse> {
        return await this.usersService.findAll();
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create or update user', operationId: 'createOrUpdateUser' })
    @ApiResponse({ status: 200, description: 'Create or update category', type: GetUserResponse })
    @HttpCode(200)
    async createOrUpdate(@Body() user: UserDto): Promise<GetUserResponse> {
        return await this.usersService.createOrUpdate(user, false);
    }

    @Delete(':id')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete user', operationId: 'deleteUser' })
    @ApiResponse({ status: 200, description: 'Delete user', type: GenericResponse })
    @HttpCode(200)
    async delete(@Param('id') userId: string): Promise<GenericResponse> {
        return await this.usersService.delete(userId);
    }
}
