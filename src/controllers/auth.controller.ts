import { Controller, Post, Body, HttpCode, Get, Param } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { AuthService } from '../services/auth.service';
import { LoginViewModel } from '../models/login-viewmodel';
import { GenericResponse } from '../models/responses/generic-response.responses';
import { RegisterRequest } from '../models/requests/register.requests';
import { GetUserResponse } from '../models/responses/user.responses';

@Controller('auth')
@ApiUseTags('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
    ) { }

    @Post('login')
    @ApiOperation({ title: 'Login user', operationId: 'login' })
    @ApiResponse({ status: 200, description: 'Login response', type: GenericResponse })
    @HttpCode(200)
    async login(@Body() loginViewModel: LoginViewModel): Promise<GenericResponse> {
        return await this.authService.login(loginViewModel);
    }

    @Post('register')
    @ApiOperation({ title: 'register', operationId: 'register' })
    @ApiResponse({ status: 200, description: 'Generic Response', type: GenericResponse })
    @HttpCode(200)
    @ApiBearerAuth()
    async register(@Body() request: RegisterRequest): Promise<GenericResponse> {
        return await this.authService.register(request);
    }

    @Get('check-token/:id')
    @ApiOperation({ title: 'check token', operationId: 'checkToken' })
    @ApiResponse({ status: 200, description: 'Generic Response', type: GetUserResponse })
    @HttpCode(200)
    @ApiBearerAuth()
    async checkToken(@Param('id') token: string): Promise<GetUserResponse> {
        return await this.authService.checkToken(token);
    }
}
