import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthToolsService } from './auth-tools.service';
import { LoginViewModel } from '../models/login-viewmodel';
import { GenericResponse } from '../models/responses/generic-response.responses';
import { AppError, AppErrorWithMessage } from '../models/app-error';
import { UsersService } from './users.service';
import { Helpers } from './tools/helpers.service';
import { RegisterRequest } from '../models/requests/register.requests';
import { UserDto } from '../models/dto/user.dto';
import { GetUserResponse } from '../models/responses/user.responses';

@Injectable()
export class AuthService {
    constructor(
        public readonly jwtService: JwtService,
        public readonly authToolsService: AuthToolsService,
        private readonly usersService: UsersService,
        private readonly helpersService: Helpers,
    ) { }

    async login(loginViewModel: LoginViewModel): Promise<GenericResponse> {
        const response: GenericResponse = new GenericResponse();
        try {
            if (!loginViewModel.password || !loginViewModel.username)
                throw AppError.getBadRequestError();

            const findUserResponse = await this.usersService.findOne({
                where: [{ username: loginViewModel.username }],
            }, true);
            if (!findUserResponse.success)
                throw new AppError(findUserResponse.error);
            if (!findUserResponse.user)
                throw new AppErrorWithMessage('Utilisateur ou mot de passe incorrect !', 403);
            if (!await this.helpersService.comparePasswords(loginViewModel.password, findUserResponse.user.password))
                throw new AppErrorWithMessage('Utilisateur ou mot de passe incorrect !', 403);
            response.token = AuthToolsService.createUserToken(this.jwtService, findUserResponse.user);
            response.success = true;
        } catch (err) {
            response.handleError(err);
        }
        return response;
    }

    async register(request: RegisterRequest): Promise<GenericResponse> {
        let response: GenericResponse = new GenericResponse();
        try {
            const userResponseUsername = await this.usersService.findOne({
                where: {
                    username: request.username,
                },
            });
            if (!userResponseUsername.success)
                throw new AppError(userResponseUsername.error);
            if (userResponseUsername.user)
                throw new AppErrorWithMessage('Un compte existe déjà avec cette identifiant !');

            const user = new UserDto();
            user.password = request.password;
            user.username = request.username;

            response = await this.usersService.createOrUpdate(user, true);
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async checkToken(token: string): Promise<GetUserResponse> {
        const response = new GetUserResponse();

        try {
            const payload = AuthToolsService.decodeToken(this.jwtService, token, false);

            if (payload && payload.username) {
                const getUser = await this.usersService.findOne({ where: { username: payload.username } });
                if (getUser.success && getUser.user) {
                    response.user = getUser.user;
                    response.success = true;
                } else {
                    response.success = false;
                }
            } else {
                response.success = false;
            }
        } catch (err) {
            response.success = false;
        }

        return response;
    }
}
