import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class UserDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    username: string;

    @ApiModelProperty()
    password: string;
}
