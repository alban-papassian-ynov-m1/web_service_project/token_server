import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class GenericResponse {
    @ApiModelProperty()
    success: boolean;
    @ApiModelPropertyOptional()
    message?: string;
    @ApiModelPropertyOptional()
    error?: any;
    @ApiModelPropertyOptional()
    token?: string;
    @ApiModelPropertyOptional()
    statusCode?: number;

    constructor() {
        this.success = false;
    }

    handleError(err) {
        this.success = false;
        this.message = err.message;
    }
}
