import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterRequest {
    @ApiModelProperty({
        description: 'username of user',
        required: false,
        type: String,
    })
    username?: string;

    @ApiModelProperty({
        description: 'password of user',
        required: false,
        type: String,
    })
    password?: string;
}
