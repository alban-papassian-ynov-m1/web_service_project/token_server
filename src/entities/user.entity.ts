import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { UserDto } from '../models/dto/user.dto';

@Entity({ name: 'users' })
export class User {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'username', nullable: false })
    username: string;

    @Column('varchar', { name: 'password', nullable: false })
    password: string;

    public toDto(getPassword?: boolean): UserDto {
        return {
            id: this.id,
            username: this.username,
            password: getPassword ? this.password : undefined,
        };
    }

    public fromDto(dto: UserDto) {
        this.id = dto.id;
        this.username = dto.username;
        // this.password = dto.password;

        if (!this.id) {
            this.id = undefined;
        }
    }
}
