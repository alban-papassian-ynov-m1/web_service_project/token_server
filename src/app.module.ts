import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from './controllers/auth.controller';
import { UsersService } from './services/users.service';
import { AuthToolsService } from './services/auth-tools.service';
import { AuthService } from './services/auth.service';
import { User } from './entities/user.entity';
import { Helpers } from './services/tools/helpers.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UsersController } from './controllers/users.controller';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 'secretjwt12345',
      signOptions: {
        expiresIn: '3650d',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'tokenserver',
      entities: [
        __dirname + '/entities/*.entity{.ts,.js}',
      ],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      User,
    ]),
  ],
  controllers: [
    AppController,
    AuthController,
    UsersController,
  ],
  providers: [
    AppService,
    UsersService,
    AuthToolsService,
    AuthService,
    Helpers,
  ],
})
export class AppModule { }
